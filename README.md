# Escape Game : La Casa de Pepper

__Auteurs__ : Julien Falgayrettes et Elisa Beaucamp  
__Projet__ : La Casa de Pepper. Escape game avec Pepper en tant que game master.  

## I. Objectifs

Réaliser un projet mettant en scène le robot Pepper, qui doit interagir avec des humains. Les consignes sont les suivantes :  
* Réaliser deux scénarios : un éthique et un non éthique  
* Utiliser la tablette du Pepper  
* Utiliser le module dialogue/qichat  
* Mettre en place un service au sens naoqi et l'interfacer avec le dialogue  
* Faire un lien avec un webservice ou créer son propre webservice  

## II. Organisation du projet

L'architecture du projet est la suivante :  
    
    .PEPPER_PROJECT_ESCAPE_GAME
    ├── SimpleWeb2
    │   ├── html
    │   │   ├── images
    │   │   │   ├── aveugle.png
    │   │   │   ├── cdp.png
    │   │   │   ├── muet.png
    │   │   │   ├── screen_OOP.png
    │   │   │   ├── rebus_non_ethique.png
    │   │   │   ├── sourd.png
    │   │   │   ├── Storyboard.png
    │   │   │   ├── toi.png
    │   │   │   └── win.png
    │   │   ├── js
    │   │   │   └── sample.js
    │   │   ├── index.html
    │   │   └── jquery-1011.2.min.js
    │   ├── api.py
    │   ├── app.py
    │   ├── audio.py
    │   ├── audio.pyc
    │   ├── forest.wav
    │   ├── getage.py
    │   ├── gun.wav
    │   ├── move.py
    │   ├── move.pyc
    │   ├── scenario_NOK.top
    │   ├── scenario_ok.top
    │   ├── Evaluation des risques.xls
    │   ├── sea.wav
    │   ├── simple_en.top
    │   ├── vision_faceDetection.py
    │   └── vision_setfacetracking.py
    └── README.md        

Tous les fichiers dont nous nous sommes servis ont été laissés dans l'arborescence. Nous avons travaillé sur tous ces fichiers séparément avant de réaliser l'intégration.   

## III. Instructions de mise en route

Pour lancer le programme principal, il faut au préalable réaliser une série de connexions avec le robot :  
* Ouvrir l'application Filezilla et se connecter au robot pepperX.local  
* Se placer dans le bon dossier côté robot et transférer les fichiers les uns après les autres  
* Ouvrir un terminal et rentrer les commandes suivantes depuis votre répertoire personnel :  

        ssh nao@pepperX.local  
        cd /home/nao/.local/share/PackageManager/apps/SimpleWeb  

* Pour lancer le programme principal, exécuter la commande suivante :  

        python app.py  

Le programme principal est une classe car nous avons décidé de programmer en orienté objet dans le main pour faciliter l'interaction entre tout les différents programmes que nous avions séparément.  

![screen_OOP](SimpleWeb2/html/images/screen_OOP.png)


## IV. Instructions d'usage

Cet escape game se joue à __trois__ personnes.  
Une fois le programme lancé, le robot Pepper s'exécute. Aucun mouvement de déplacement n'a été codé, il n'est donc pas nécessaire de le débrancher
pour lui permettre de bouger. La seule consigne de mouvement a été réalisé sur son bras droit qui va se relever à 90 degrés par rapport au sol,
donc il faut s'arranger pour ne pas gêner cet endroit.  
Quand le choix du rôle est effectué par l'utilisateur, le programme de détection d'âge se lance. Il faut rester devant le robot jusqu'à ce que la 
question __Reste-t-il un autre rôle à définir ?__ s'affiche sur la tablette de Pepper. L'âge deviné s'affiche dans le terminal de l'ordinateur.  
Lors du choix des rôles, il faut que les trois joueurs passent. Ils doivent être au courant du choix du joueur précédent pour ne pas reprendre le même rôle
et éviter des confusions.  

## V. Scénario

### 1. Storyboard

Avant de commencer le projet, nous avons dessiné un premier storyboard en fonction de nos idées. Le storyboard fournit au professeur ne correspond plus exactement
à ce que nous avons réalisé, car nous avons soit été trop ambitieux, soit manqué de temps, soit changé d'avis. Voici donc le nouveau storyboard qui correspond
exactement au projet rendu :  

![Storyboard](SimpleWeb2/html/images/Storyboard.png)

### 2. Détails de l'escape game  

Le projet que nous avons réalisé est une simulation d'escape game, dont le robot Pepper est le game master. Au lancement du programme, dès que le robot détecte une tête, 
il dit la température du jour (utilisation d'un webservice). Pour démarrer l'escape game, il faut dire __Pirouette__ à Pepper (utilisation du module dialogue).  
Dans un premier temps, vous vous retrouvez devant un dilemme à travers une mise en situation qui n'est pas énoncée par le robot, le texte étant un peu long, on peut seulement le lire sur la tablette. Cette situation permet de définir si le scénario sera éthique ou non. En effet, dès lors qu'au moins un des trois participants choisit de se sauver lui-même du bâtiment en feu, le scénario ne sera pas éthique. Il faut que les trois joueurs choisissent un rôle (utilisation de la tablette). Nous n'avons pas eu le temps de mettre en place un algorithme de décision des rôles en fonction de ce que les joueurs ont choisit. Il faut donc que les joueurs soient au courant du choix des participants précédents pour ne pas qu'il y ait de doublan. Si sur un des trois, le choix __se sauver soit-même__ a été fait, alors les rôles restants sont redistribués.  
Pendant le choix des rôles, le robot essaye de déterminer l'âge de la personne en face de lui (utlisation d'une API au sens naoqi). Le but initial était de déterminer le prix de l'escape game en fonction des âges (prix étudiant par exemple), mais nous n'avons pas eu le temps de le développer.  
Une fois que tous les rôles ont été choisit, le scénario d'escape game se lance. Vous êtes des braqueurs de banque dont le but est de trouver le code du coffre-fort. Il faut trouver deux chiffres pour débloquer le code d'accès au coffre-fort de la banque. Le code à trouver est le même quel que soit le scénario lancé. Les différences entre les deux scénarios sont les suivantes :   
* __Scénario éthique__ : Une fois le scénario d'escape game lancé, le rébus qui permet de trouver le code s'affiche. Si vous ne trouvez pas, vous pouvez cliquer sur le bouton d'indice qui vous donnera une bonne indication. Si vous avez faux, le robot ba gentiement vous proposer de réessayer. Si vous avez bon, les trois joueurs pourront se partager le butin.  
* __Scénario non éthique__ : Pour ce scénario, vous aurez le même affichage que pour celui éthique. Cependant, que vous cliquiez sur le bouton indice ou sur la mauvaise réponse, le robot vous dira que vous êtes idiot et qu'il faut faire un effort. Si vous avez la bonne réponse, alors le robot va commencer son geste du bras droit en vous disant que vous êtes un de trop pour pouvoir vous partager le butin, donc il va vous tirer dessus en faisant approximativement un geste de pistolet avec son bras et en exécutant un brut de tir de pistolet, un peu en retard par rapport au bras levé puisque le Pepper a du mal a exécuter plusieurs programmes en même temps (service au sens naoqi interfacé avec le dialogue).  
  
Le rébus à trouver est le suivant :  

![Rébus](SimpleWeb2/html/images/rebus_non_ethique.png)

### 3. Lien Youtube de démonstration

Vous pouvez consulter une démonsration de notre projet en suivant le lien Youtube ci-dessous :  

        https://youtu.be/jnk5AkNheag  

Seul le scénario non-éthique a été filmé, le scénario éthique n'apportant rien de particulier.

## VI. Ethique

Les choix non-éthique ont été forts . Le robot choisit sciemment de porter atteinte à la vie d'un humain, action qui ne respecte pas la première loi d'Asimov, que nous pouvons retrouvée énoncée dans le rapport de la COMEST, article 17 page 16 :

        Un robot ne peut porter atteinte à un être humain, ni, en restant passif, permettre qu'un être humain soit exposé au danger.  

Aussi, nous avons fait le choix de ne pas aider l'humain qui clique sur __indice__ alors qu'il doit trouver le code pour le coffre-fort. Cela ne respecte pas le premier article de la comission Tricot que l'on peut retrouver dans le rapport de la CNIL, à la page 45, qui commence par la phrase suivante : 

        L’informatique doit être au service de chaque citoyen.  

Enfin, nous pouvons nous questionner quant à l'obtention d'une estimation d'âge de l'utilisateur sans-même que nous le tenions au courant. Cela peut rentrer en contradiction avec un article provennant d'un rapport d'Olivier Ezratty qui précise :  

        L’IA explicable doit aussi répondre à des exigences d’éthique (biais, ...), de confiance (équité, rigueur, ...) et de sécurité (respect de la vie privée, non détournement des données personnelles pour des usages non documentés, ...)  

## VII. Bonnes pratiques

Le projet a été codé à l'aide de Python 2 et Python 3, suivant les différentes biliothèques, les programmes fournit par les professeurs et la machine utilisée pouir écrire les programmes. Les codes ont été notés par le package Pylint qui se fonde sur les règles de la PEP8. Certaines erreur non résolvables avec nos connaissances ont empêché le bon fonctionnement de ce package.
Les commentaires sont en anglais, les docstrings sont écrites de la manière la plus détaillée possible. 


## VIII. Ressources

énoncé TP : https://gitlab.com/t8947/ihr  

PEP8 :  https://www.python.org/dev/peps/pep-0008/  
 
Pylint : https://realpython.com/python-code-quality/  

Tous les dessins ont été réalisés par nos soins sur Paint pour éviter tout problème de droits.  
