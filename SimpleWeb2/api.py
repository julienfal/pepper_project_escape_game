'''
@authors : Julien Falgayrettes Elisa Beaucamp
@date : 12/04/22
@obj : recovering of the current temperature in Lyon
'''

import json
import urllib2
#import requests

#setting keys and geo infos about Lyon
API_KEY = "38843eff9addea0e39391ee2c8e87927"
LAT = "45.764043"
LON = "4.835659"

#loading url and opening doc in json format
URL="http://api.openweathermap.org/data/2.5/weather?lat="\
    +LAT+"&lon="+LON+"&appid=38843eff9addea0e39391ee2c8e87927"
weatherbot=urllib2.urlopen(URL)
weatherinfo = json.load(weatherbot)

#recovery of the infos by browsing the json
current_temp=weatherinfo["main"]["temp"]-273.15
