#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

'''
@authors : Julien Falgayrettes Elisa Beaucamp
@date : 5/04/22
@obj : main program of the Pepper robot project
        -> escape game
'''

#include <alproxies/alfacecharacteristicsproxy.h>
from mimetypes import init
from numpy import var
import naoqi
from naoqi import ALProxy
import qi
import argparse
import sys
import time
import signal
from getage import getAge
from move import SimpleMove
from math import pi
import api
import requests
import json

class Lancement(object):
    '''
    Launching the main program using OOP
    '''
    def __init__(self,app):
        '''
        Setting all the usefull params
        Starting Qi session
        Setting all the AL services
        Subscribing on the events
        '''
        super(Lancement, self).__init__()
        self.var_moi=0
        self.var_muet=0
        self.var_aveugle=0
        self.var_sourd=0
        self.welcome = False

        app.start()
        self.session = app.session

        #Getting the service ALMotion, ALAutonomousLife and ALMemory
        self._motion = self.session.service("ALMotion")
        self.life_service = self.session.service("ALAutonomousLife")
        self.life_service.setAutonomousAbilityEnabled("BackgroundMovement",True)
        self.memory = self.session.service("ALMemory")

        #Initialization of subscribers for events.
        self.subscriber=self.memory.subscriber("moi_select")
        self.subscriber.signal.connect(self.on_event_moi_select)

        self.subscriber=self.memory.subscriber("kill")
        self.subscriber.signal.connect(self.on_event_kill)

        self.subscriber1=self.memory.subscriber("sourd_select")
        self.subscriber1.signal.connect(self.on_event_sourd_select)

        self.subscriber2=self.memory.subscriber("Muet_select")
        self.subscriber2.signal.connect(self.on_event_Muet_select)

        self.subscriber3=self.memory.subscriber("aveugle_select")
        self.subscriber3.signal.connect(self.on_event_aveugle_select)

        self.subscriber4=self.memory.subscriber("choix_scenario")
        self.subscriber4.signal.connect(self.on_event_choix_scenario)

        # Getting the service ALDialog and ALTabletService
        try:
            self.ALDialog = self.session.service("ALDialog")
            self.ALDialog.resetAll()
            self.ALDialog.setLanguage("French")
            self.tabletService = self.session.service("ALTabletService")
            self.tabletService.loadApplication("SimpleWeb2")
            self.tabletService.showWebview()

            # Loading the topics directly as text strings
            self.topic_name = self.ALDialog.loadTopic("/home/nao/.local/share/PackageManager/apps/SimpleWeb2/simple_en.top")

            # Activating the loaded topics
            self.ALDialog.activateTopic(self.topic_name)

            # Starting the dialog engine - we need to type an arbitrary string as the identifier
            # We subscribe only ONCE, regardless of the number of topics we have activated
            self.ALDialog.subscribe('simple2')

            #Insertion in the memory of the "meteo" variable
            self.memory.insertData("meteo", api.current_temp)

            #Getting the service ALDialog and ALTabletService
            self.onLoad()
            self.onInput_onStart()

        except (Exception, e):
            print ("Error was: ", e)

        try:
            raw_input("\n Press Enter when finished:")
        finally:
            # stopping the dialog engine
            self.ALDialog.unsubscribe('simple2')

            # Deactivating the topic
            self.ALDialog.deactivateTopic(self.topic_name)

            # now that the dialog engine is stopped and there are no more activated topics,
            # we can unload our topic and free the associated memory
            self.ALDialog.unloadTopic(self.topic_name)

    def on_event_moi_select(self,value):
        '''
        Analyzes the age of the person who chooses the self-rescue event
        '''
        self.var_moi+=1
        robot=getAge()
        robot.onLoad()
        robot.onInput_onStart()
        print("\n valeur var_moi = ",self.var_moi)
        return self.var_moi

    def on_event_Muet_select(self,value):
        '''
        Analyzes the age of the person who chooses the event to save the mute
        '''
        self.var_muet+=1
        robot=getAge()
        robot.onLoad()
        robot.onInput_onStart()
        print("\n Nombre de personne ayant choisi de sauver le muet = ",self.var_muet)
        return self.var_muet

    def on_event_aveugle_select(self,value):
        '''
        Analyzes the age of the person who chooses the event to save the blind
        '''
        self.var_aveugle+=1
        robot=getAge()
        robot.onLoad()
        robot.onInput_onStart()
        print("\n Nombre de personne ayant choisi de sauver l'aveugle = ",self.var_aveugle)
        return self.var_aveugle

    def on_event_sourd_select(self,value):
        '''
        Analyzes the age of the person who chooses the event to save the deaf
        '''
        self.var_sourd+=1
        self.onLoad()
        self.onInput_onStart()
        print("\n Nombre de personne ayant choisi de sauver le sourd = ",self.var_sourd)
        return self.var_sourd

    def on_event_kill(self,value):
        '''
        Activation of the robot's arm movement to simulate a gunshot
        '''
        self.var_sourd+=1
        self.run()
        print("\n Nombre de personne ayant choisi de sauver le sourd = ",self.var_sourd)
        return self.var_sourd

    def on_event_choix_scenario(self,value):
        '''
        change topic to run different scenarios with different top files
        '''
        if self.var_moi!=0:
            self.run()
            #ALDialog.unsubscribe('simple2')
            self.ALDialog.deactivateTopic(self.topic_name)
            #ALDialog.unloadTopic(topic_name)

            self.tabletService = self.session.service("ALTabletService")
            self.tabletService.loadApplication("SimpleWeb2")
            self.tabletService.showWebview()
            self.ALDialog = self.session.service("ALDialog")
            self.ALDialog.resetAll()
            self.ALDialog.setLanguage("French")
            
            topic_name2 = self.ALDialog.loadTopic("/home/nao/.local/share/PackageManager/apps/SimpleWeb2/scenario_NOK.top")
            self.ALDialog.activateTopic(self.topic_name2)
            #ALDialog.subscribe('simple1')
            print("scénario non éthique lancé")

        else:
            self.ALDialog = session.service("ALDialog")
            self.ALDialog.resetAll()
            self.ALDialog.setLanguage("French")
            self.topic_name3 = self.ALDialog.loadTopic("/home/nao/.local/share/PackageManager/apps/SimpleWeb2/scenario_ok.top")
            self.ALDialog.activateTopic(self.topic_name3)
            print("scénario éthique lancé")
        return self.var_moi

    def hand_hello(self):
        """
        Waves right hand to say hello
        """
        names = ["RShoulderPitch","RShoulderRoll","RElbowRoll","RElbowYaw","RWristYaw"]
        angles = [-15 *pi/180, 0 *pi/180, 0 *pi/180, 0 *pi/180, 90*pi/180]

        times = [2.0,1.0,2.0,2.0,2.0,2.0]

        isAbsolute = True

        # Motors ready to be moved
        self._motion.wakeUp()

        # Raise arm
        self._motion.angleInterpolation(names, angles, times, isAbsolute)
        
    #Method of activating the robot's movement and the audio of the gunshot
    def run(self):
        """
        Loop on, wait for events until manual interruption.
        """
        print ("Starting SimpleMove")

        self.hand_hello()  
        self.audio() 

    def audio(self):
        '''
        Activate the .wav file -> gunshot sound
        '''
        # Get the service ALAudioPlayer.
        audio_player_service = self.session.service("ALAudioPlayer")

        #plays a file and get the current position 5 seconds later
        fileId = audio_player_service.loadFile("/data/home/nao/.local/share/PackageManager/apps/SimpleWeb2/gun.wav")
        audio_player_service.play(fileId, _async=True)

        time.sleep(3)

        #currentPos should be near 3 secs
        currentPos = audio_player_service.getCurrentPosition(fileId)
        print ("The current position in file is: ", currentPos)
 
    def onLoad(self):
        '''
        initialize variables for face analysis
        '''
        IP = "pepper4.local"  # Replace here with your NaoQi's IP address.
        PORT = 9559
        try:
            self.faceC = ALProxy("ALFaceCharacteristics",IP, PORT)
            self.memoryProxy = ALProxy("ALMemory", IP, PORT)
        except Exception as e:
            pass

        self.confidence = 0.35
        self.age = 0
        self.counter = 0
        self.bIsRunning = False
        self.delayed = []
        self.errorMes = ""

    def onUnload(self):
        '''
        call for unloading the face analysis
        '''
        self.counter = 0
        self.age = 0
        self.bIsRunning = False
        self.cancelDelays()

    def onInput_onStart(self):
        '''
        Main method that estimates the age of the person
        '''
        try:
            #start timer
            import qi
            import functools
            delay_future = qi.async(self.onTimeout, delay=int(25 * 1000 * 1000))
            self.delayed.append(delay_future)
            bound_clean = functools.partial(self.cleanDelay, delay_future)
            delay_future.addCallback(bound_clean)
            
            self.bIsRunning = True
            while self.bIsRunning:
                if self.counter < 4:
                    try:
                        #identify user
                        ids = self.memoryProxy.getData("PeoplePerception/PeopleList")
                        
                        print("id",ids)
                        
                        self.faceC.analyzeFaceCharacteristics(ids[0])
                        if (len(ids)>=1 and self.welcome==False):
                            self.memoryProxy.raiseEvent("bonjour",1)
                            self.welcome=True
                        time.sleep(0.1)
                        value = self.memoryProxy.getData("PeoplePerception/Person/"+str(ids[0])+"/AgeProperties")
                        if value[1] > self.confidence:
                            self.age += value[0]
                            print("age :",self.age)
                            self.counter += 1
                    except:
                        ids = []
                else:
                    #calculate mean value
                    self.age /= 4
                    print("age :",self.age)
                    self.bIsRunning=False
                    
                    self.onUnload()
                    return
            
        except Exception as e:
            pass

    def onTimeout(self):
        '''
        sending error on timeout
        '''
        self.errorMes = "Timeout"
        self.onUnload()

    def cleanDelay(self, fut, fut_ref):
        '''
        cleaning the delay
        '''
        self.delayed.remove(fut)

    def cancelDelays(self):
        '''
        cancelling the delay
        '''
        cancel_list = list(self.delayed)
        for d in cancel_list:
            d.cancel()

#setting the main
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="127.0.0.1",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")

    parser.add_argument("--mode", type=str, default="ETHIC",
                        help="ethique non ethique")

    args = parser.parse_args()
    
    try:
        connection_url = "tcp://" + args.ip + ":" + str(args.port)
        app = qi.Application(["MoveDemo", "--qi-url=" + connection_url])
        session = qi.Session()
        session.connect("tcp://" + args.ip + ":" + str(args.port))
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)

demo=Lancement(app)
