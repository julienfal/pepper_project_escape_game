#! /usr/bin/env python
# -*- encoding: UTF-8 -*-

'''
@authors : Julien Falgayrettes Elisa Beaucamp
@date : 5/04/22
@obj : launches the gunshot audio
'''

import argparse
import sys
import time
import qi

def main(session):
    """
    Uses the getCurrentPosition method
    """
    # Get the service ALAudioPlayer.
    audio_player_service = session.service("ALAudioPlayer")

    #plays a file and get the current position 5 seconds later
    file_id = audio_player_service.loadFile\
        ("/data/home/nao/.local/share/PackageManager/apps/SimpleWeb2/gun.wav")
    audio_player_service.play(file_id, _async=True)

    time.sleep(3)

    #currentPos should be near 3 secs
    current_pos = audio_player_service.getCurrentPosition(file_id)
    print ("The current position in file is: ", current_pos)

#setting the main
if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="127.0.0.1",
                        help="Robot IP address. On robot or Local Naoqi: use '127.0.0.1'.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Naoqi port number")

    args = parser.parse_args()
    session = qi.Session()
    try:
        session.connect("tcp://" + args.ip + ":" + str(args.port))
    except RuntimeError:
        print ("Can't connect to Naoqi at ip \"" + args.ip + "\" on port " + str(args.port) +".\n"
               "Please check your script arguments. Run with -h option for help.")
        sys.exit(1)
    main(session)
