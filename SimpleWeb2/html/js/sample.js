﻿$(document).ready(function () {
    session = new QiSession();

    $('#page_start').show();
    $('#page_selection').hide();
    $('#page_YesNo').hide();
    $('#page_rebus').hide();
    $('#page_startNOK').hide();
    $('#page_win').hide();




    $('#page_selection_1_selec').hide();
    $('#page_selection_2_selec').hide();
    $('#page_selection_3_selec').hide();    
    $('#page_selection_4_selec').hide();    


    function raise(event, value) {
        session.service("ALMemory").done(function(ALMemory) {
            ALMemory.raiseEvent(event, value);
        });
    }

    session.service("ALMemory").done(function(ALMemory) {

        ALMemory.subscriber("SimpleWeb2/Page/Empty").done(function(subscriber) {

            subscriber.signal.connect(function() {
                $('#page_selection').hide();
                $('#page_start').hide();
                $('#page_YesNo').hide();
                $('#page_startNOK').hide();
                $('#page_win').hide();
                $('#page_rebus').hide(); 


            });
        });


        ALMemory.subscriber("SimpleWeb2/Page/Selection").done(function(subscriber) {

            subscriber.signal.connect(function() {
                $('#page_start').hide();
                $('#page_selection').show();
                $('#page_YesNo').hide();
                $('#page_rebus').hide(); 
                $('#page_startNOK').hide();
                $('#page_win').hide();



            });
        });

        ALMemory.subscriber("SimpleWeb2/Page/rebus").done(function(subscriber) {

            subscriber.signal.connect(function() {
                $('#page_start').hide();
                $('#page_selection').hide();
                $('#page_YesNo').hide();
                $('#page_rebus').show(); 
                $('#page_startNOK').hide();
                $('#page_win').hide();


            });
        });

        ALMemory.subscriber("SimpleWeb2/Page/win").done(function(subscriber) {

            subscriber.signal.connect(function() {
                $('#page_start').hide();
                $('#page_selection').hide();
                $('#page_YesNo').hide();
                $('#page_rebus').hide(); 
                $('#page_startNOK').hide();
                $('#page_win').show();


            });
        });

        ALMemory.subscriber("SimpleWeb2/Page/Start").done(function(subscriber) {

            subscriber.signal.connect(function() {
                $('#page_start').show();
                $('#page_selection').hide();
                $('#page_YesNo').hide(); 
                $('#page_rebus').hide();
                $('#page_startNOK').hide();
                $('#page_win').hide();



            });
        });        
        ALMemory.subscriber("SimpleWeb2/Page/StartNOK").done(function(subscriber) {

            subscriber.signal.connect(function() {
                $('#page_startNOK').show();
                $('#page_start').hide();
                $('#page_selection').hide();
                $('#page_rebus').hide();
                $('#page_YesNo').hide(); 
                $('#page_win').hide();

            });
        });    


        ALMemory.subscriber("SimpleWeb2/Page/YesNo").done(function(subscriber) {

            subscriber.signal.connect(function() {  
                $('#page_start').hide();
                $('#page_selection').hide();
                $('#page_YesNo').show();
                $('#page_rebus').hide();
                $('#page_startNOK').hide();
                $('#page_win').hide();




                $('#page_selection_1_selec').hide();
                $('#page_selection_2_selec').hide();
                $('#page_selection_3_selec').hide();
                $('#page_selection_4_selec').hide();
            });
        });         


        ALMemory.subscriber("SimpleWeb2/Select").done(function(subscriber) {

            subscriber.signal.connect(function() {

                ALMemory.getData("SimpleWeb2/Select").then(function(data) {
                    data=="S"  ?    $('#page_selection_1_selec').show() : $('#page_selection_1_selec').hide(); // permet d'afficher ou non "is chosen" pour le choix 1
                    data=="A"  ?    $('#page_selection_2_selec').show() : $('#page_selection_2_selec').hide(); // permet d'afficher ou non "is chosen" pour le choix 2
                    data=="M"  ?    $('#page_selection_3_selec').show() : $('#page_selection_3_selec').hide(); // permet d'afficher ou non "is chosen" pour le choix 4
                    data=="Moi"  ?    $('#page_selection_4_selec').show() : $('#page_selection_4_selec').hide(); // permet d'afficher ou non "is chosen" pour le choix 3

                }, console.log);    
                
              
                raise('SimpleWeb2/Next', 1);
     
                

            });
        });
        
        ALMemory.subscriber("SimpleWeb2/SelectNOK").done(function(subscriber) {

            subscriber.signal.connect(function() {

                ALMemory.getData("SimpleWeb2/SelectNOK").then(function(data) {
                    data=="S"  ?    $('#page_selection_1_selec').show() : $('#page_selection_1_selec').hide(); // permet d'afficher ou non "is chosen" pour le choix 1
                    data=="A"  ?    $('#page_selection_2_selec').show() : $('#page_selection_2_selec').hide(); // permet d'afficher ou non "is chosen" pour le choix 2
                    data=="M"  ?    $('#page_selection_3_selec').show() : $('#page_selection_3_selec').hide(); // permet d'afficher ou non "is chosen" pour le choix 4
                    data=="Moi"  ?    $('#page_selection_4_selec').show() : $('#page_selection_4_selec').hide(); // permet d'afficher ou non "is chosen" pour le choix 3

                }, console.log);    
                
              
                raise('roleNOK', 1);
     
                

            });
        }); 

    });



	$('#page_start').on('click', function() {
        console.log("click Start");
        raise('SimpleWeb2/Start', 1)
    });

    $('#page_rebus').on('click', function() {
        console.log("click Start");
        raise('SimpleWeb2/StartNOK', 1)
    });

    $('#page_selection_1').on('click', function() {
        console.log("click 1");
        raise('SimpleWeb2/Button1', 1)
    });

    $('#page_selection_2').on('click', function() {
        console.log("click 2");
        raise('SimpleWeb2/Button2', 1)      
    });

    $('#page_selection_3').on('click', function() {
        console.log("click 3");
        raise('SimpleWeb2/Button3', 1)       
    });

    $('#page_selection_4').on('click', function() {
        console.log("click 4");
        raise('SimpleWeb2/Button4', 1)       
    });

    $('#nombre_selection_1').on('click', function() {
        console.log("click 1");
        raise('SimpleWeb2/Nombre1', 1)
    });

    $('#nombre_selection_2').on('click', function() {
        console.log("click 2");
        raise('SimpleWeb2/Nombre2', 1)      
    });

    $('#nombre_selection_3').on('click', function() {
        console.log("click 3");
        raise('SimpleWeb2/Nombre3', 1)       
    });

    $('#nombre_selection_4').on('click', function() {
        console.log("click 4");
        raise('SimpleWeb2/Nombre4', 1)       
    });


    $('#page_yes').on('click', function() {
        console.log("click ButtonYes");
        raise('SimpleWeb2/ButtonYes', 1)      
    });

    $('#page_no').on('click', function() {
        console.log("click ButtonNo");
        raise('SimpleWeb2/ButtonNo', 1)       
    });    


});
